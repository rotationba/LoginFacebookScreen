import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  componentWillMount() {
    StatusBar.setHidden(true);
  }
  changeEmail = email => {
    this.setState({ email });
  };
  changePassword = password => {
    this.setState({ password });
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center'}} >
          <Text style={styles.txtFb}>facebook</Text>
        </View>
        <View style={{ flex: 6, justifyContent: 'space-between'}} >
          <View>
            <TextInput
              style={styles.inputStyle}
              placeholder={"Email or Phone"}
              underlineColorAndroid={'transparent'}
              onChangeText={this.changeEmail}
              value={this.state.email}
            />
            <TextInput
              style={styles.inputStyle}
              placeholder={"Password"}
              underlineColorAndroid={'transparent'}
              onChangeText={this.changePassword}
              value={this.state.password}
              secureTextEntry
            />
            <TouchableOpacity style={styles.btnLogin}>
              <Text style={styles.txtLogin}>Log In</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignSelf: "stretch",
              alignItems: "center"
            }}
          >
            <View style={{ width: 35, height: 35 }} />
            <Text style={styles.txtSignUp}>Sign Up for Facebook</Text>
            <TouchableOpacity style={styles.btnHelp}>
              <Text style={styles.txtHelp}>?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4267b2",
    padding: 15
  },
  txtFb: {
    color: "#fff",
    fontSize: 60,
    fontWeight: "bold",
  },
  inputStyle: {
    backgroundColor: "#fff",
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#8e8e93",
    color: "black",
    alignSelf: "stretch",
    padding: 12,
    marginVertical: 10,
    height: 40,
  },
  btnLogin: {
    backgroundColor: "#153677",
    alignSelf: "stretch",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    marginTop: 10
  },
  txtLogin: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold"
  },
  btnHelp: {
    backgroundColor: "#153677",
    width: 35,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5
  },
  txtHelp: {
    color: "#fff",
    fontSize: 25,
    fontWeight: "bold"
  },
  txtSignUp: {
    color: "#fff",
    fontSize: 18,
    textDecorationLine: "underline"
  }
});
